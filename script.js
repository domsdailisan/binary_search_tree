//Binary Search Tree(BST) - comprised of data points called nodes
//The first node (the top of the tree) is called the root node
//BST can only have two branches per node(1parent, 2max children)
//nodes with no children are called leaves
//ordered such as each subtree is less than or equal to the parent node
//each right subtree is greater than or equal to the parent node
//due to its binary nature, operations will be able to skip about half of the Tree, making lookups, insertions, and deletions are faster than those performed arrays

let BST = function () {
	//create a root property which will initially be equal to null
	this.root = null

	let Node = function(data) {
		this.data = data
		this.left = null
		this.right = null
	}

	//this method will add a node to our bst using a passed data
	this.add = function(data) {
		// console.log(data)
		let node = this.root
		//if this is the first node to be added hence node === null
		if(node === null){
			this.root = new Node(data)
			return
		}else {
			let searchTree = function(node){
				//if data to be added is loess than the current root
				if(data < node.data){
					//check if the root has no left child yet
					if(node.left === null) {
						node.left = new Node(data)
						return
					}else if(node.left !== null){
						return searchTree(node.left) //recursive. function inside a function
 					}
				}else if(data > node.data){
					if(node.right === null){
						node.right = new Node(data)
						return
					}else if(node.right !== null){
						return searchTree(node.right)//recursive
					}
				}else {
					return null
				}
			}
			return searchTree(node)
		}
	} // end of method add

	//this method will look on the bst to search for the minimum value
	this.findMin = function() {
		//begin searching from the root node by setting the root node as current node
		let currentNode = this.root
		while(currentNode.left !== null){
			currentNode = currentNode.left
		}
		return currentNode.data
	}//end of findMin

	this.findMax = function() {
		//begin searching from the root node by setting the root node as current node
		let currentNode = this.root
		while(currentNode.right !== null){
			currentNode = currentNode.right
		}
		return currentNode.data
	}//end of findMax

	//this method will look on the bst to search fo the specific node
	this.find = function(data) {
		//begin searching from the root node by setting the root node as current
		let current = this.root
		while(current.data !== data){
			if (data < current.data){
				//if it is, traverse the tree leftwards until it reaches the data to be the current node
				current = current.left
			}else {
				current = current.right
			}
			if(current === null){
				return null
			}
		}
		return current
	}//end of find

	//this method will return true or false if passed in data is on the trea or not
	this.isPresent = function(data){
		let current = this.root
		while(current) {
			if(data === current.data){
				return true
			}
			if(data < current.data){
				current = current.left
			}else {
				current = current.right
			}
		}
		return false
	}//end of isPresent

	//this method will remove the data/node in our bst
	this.remove = function(data) {
		let removeNode = function(node, data){
			//check first if we have an empty tree
			if(node == null) {
				return null
			}

			//if the tree is not empty
			if(data == node.data){
				//check if the node has no children
				if(node.left == null && node.right == null){
					return null
				}
				//if the node has no left child
				if(node.left == null){
					return node.right
				}
				//if the node has no right child
				if(node.right == null){
					return node.left
				}

				//if node has two children, we have to go to immediate right and traverse leftwards. go to the right child of this node and set that as a temporary node
				let tempNode = node.right
				//while thempnode has a left child
				while(tempNode.left !== null){
					//traverse the tree leftwards by assigning the tempNode's left child as the new tempNode
					tempNode = tempNode.left
				}
				//once we've reached the last node to the left, replace the node to be removed with the data of the leftmost node
				node.data = tempNode.data
				node.right = removeNode(node.right, tempNode.data)
				return node
			} else if(data < node.data) {
				//check if the data were looking for tis less than the data of the
				node.left = removeNode(node.left, data)
				return node
			} else {
				node.right = removeNode(node.right, data)
				return node
			}
		}
		//begin looking for the node to be removed by calling in ur function and apssed in the arguments
		this.root = removeNode(this.root, data)
	}

}//end of BST function

let bst = new BST()
// console.log(bst) //BST { root: null }
bst.add(4) //75
bst.add(2)
bst.add(6)
bst.add(1)
bst.add(3)
bst.add(5)
bst.add(7)
bst.remove(4)
console.log(bst)

//2 types of Web Architecture
/*
	Monolithic -
	Microservices -

*/
